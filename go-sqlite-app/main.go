package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/Masterminds/squirrel"
	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/sqlite3"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/mattn/go-sqlite3"
)

type TestStruct struct {
	ID     string `db:"id" json:"id"`
	Value  string `db:"value" json:"value"`
	Source string `db:"source" json:"source"`
}

func (c *Controller) getTest(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	res := make([]TestStruct, 0)

	q, _, err := squirrel.Select("id", "value", "source").From("test").ToSql()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot create query: %v", err)
		return
	}

	err = c.db.SelectContext(ctx, &res, q)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot select: %v", err)
		return
	}

	data, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot marshal json: %v", err)
		return
	}

	w.Write(data)
}

func (c *Controller) postTest(w http.ResponseWriter, r *http.Request) {
	defaultTestItem := TestStruct{
		ID:     uuid.NewString(),
		Value:  time.Now().String(),
		Source: r.RemoteAddr,
	}

	var testItem TestStruct
	err := json.NewDecoder(r.Body).Decode(&testItem)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot decode req body: %v", err)
		return
	}
	if testItem.Value == "" && testItem.ID == "" {
		testItem = defaultTestItem
	}
	testItem.ID = defaultTestItem.ID

	sql, args, err := squirrel.Insert("test").
		Columns("id", "value", "source").
		Values(testItem.ID, testItem.Value, testItem.Source).
		PlaceholderFormat(squirrel.Dollar).ToSql()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot create query: %v", err)
		return
	}

	_, err = c.db.Exec(sql, args...)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot insert: %v", err)
		return
	}

	data, err := json.Marshal(testItem)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot marshal json: %v", err)
		return
	}

	w.Write(data)
}

func (c *Controller) getWelcome(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("welcome from go-sqlite-app"))
}

type Controller struct {
	cli *http.Client
	db  *sqlx.DB
}

func main() {
	db, err := sqlx.Connect("sqlite3", "db/test.db")
	if err != nil {
		log.Fatal("cannot open sql:", err)
	}
	driver, err := sqlite3.WithInstance(db.DB, &sqlite3.Config{})
	if err != nil {
		log.Fatal("cannot create driver:", err)
	}
	m, err := migrate.NewWithDatabaseInstance("file://migrations", "sqlite3", driver)
	if err != nil {
		log.Fatal("cannot create db instance:", err)
	}
	err = m.Up()
	if err != nil && !strings.Contains(err.Error(), "no change") {
		log.Fatal("cannot migrate db:", err)
	}

	c := &Controller{
		cli: &http.Client{},
		db:  db,
	}

	r := chi.NewRouter()
	r.Get("/", c.getWelcome)
	r.Get("/test", c.getTest)
	r.Post("/test", c.postTest)
	http.ListenAndServe(":80", r)
}
