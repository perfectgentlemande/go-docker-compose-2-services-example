module bitbucket.org/perfectgentlemande/go-docker-compose-2-services-example/go-sqlite-app

go 1.17

require (
	github.com/Masterminds/squirrel v1.5.2
	github.com/go-chi/chi/v5 v5.0.7
	github.com/golang-migrate/migrate/v4 v4.15.1
	github.com/google/uuid v1.3.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/mattn/go-sqlite3 v1.14.11
)

require (
	github.com/hashicorp/errwrap v1.0.0 // indirect
	github.com/hashicorp/go-multierror v1.1.0 // indirect
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0 // indirect
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	go.uber.org/atomic v1.6.0 // indirect
)
