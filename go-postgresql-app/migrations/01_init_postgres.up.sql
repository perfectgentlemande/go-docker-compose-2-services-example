CREATE TABLE test (
    id          uuid PRIMARY KEY,
    value       character varying NOT NULL,
    source      character varying NOT NULL
);