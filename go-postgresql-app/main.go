package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	"strings"
	"time"

	"github.com/Masterminds/squirrel"
	"github.com/go-chi/chi/v5"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/google/uuid"
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"
)

type TestStruct struct {
	ID     string `db:"id" json:"id"`
	Value  string `db:"value" json:"value"`
	Source string `db:"source" json:"source"`
}

func (c *Controller) getTest(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	res := make([]TestStruct, 0)

	q, _, err := squirrel.Select("id", "value", "source").From("test").ToSql()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot create query: %v", err)
		return
	}

	err = c.db.SelectContext(ctx, &res, q)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot select: %v", err)
		return
	}

	data, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot marshal json: %v", err)
		return
	}

	w.Write(data)
}

func (c *Controller) postTest(w http.ResponseWriter, r *http.Request) {
	defaultTestItem := TestStruct{
		ID:     uuid.NewString(),
		Value:  time.Now().String(),
		Source: r.RemoteAddr,
	}

	var testItem TestStruct
	err := json.NewDecoder(r.Body).Decode(&testItem)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot decode req body: %v", err)
		return
	}
	if testItem.Value == "" && testItem.ID == "" {
		testItem = defaultTestItem
	}
	testItem.ID = defaultTestItem.ID

	sql, args, err := squirrel.Insert("test").
		Columns("id", "value", "source").
		Values(testItem.ID, testItem.Value, testItem.Source).
		PlaceholderFormat(squirrel.Dollar).ToSql()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot create query: %v", err)
		return
	}

	_, err = c.db.Exec(sql, args...)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot insert: %v", err)
		return
	}

	data, err := json.Marshal(testItem)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot marshal json: %v", err)
		return
	}

	w.Write(data)
}

func (c *Controller) getTestSQLite(w http.ResponseWriter, r *http.Request) {
	newReq, err := http.NewRequest(
		http.MethodGet, "http://go-sqlite-app:80/test", nil,
	)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot create request to go-sqlite-app: %v", err)
		return
	}

	resp, err := c.cli.Do(newReq)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot do request to go-sqlite-app: %v", err)
		return
	}
	defer resp.Body.Close()

	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot read response body: %v", err)
		return
	}

	if resp.StatusCode != http.StatusOK {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "something wrong: %s", string(bodyBytes))
		return
	}

	w.Write(bodyBytes)
}

func (c *Controller) postTestSQLite(w http.ResponseWriter, r *http.Request) {
	newReq, err := http.NewRequest(
		http.MethodPost, "http://go-sqlite-app:80/test", r.Body,
	)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot create request to go-sqlite-app: %v", err)
		return
	}

	resp, err := c.cli.Do(newReq)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot do request to go-sqlite-app: %v", err)
		return
	}
	defer resp.Body.Close()

	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "cannot read response body: %v", err)
		return
	}

	if resp.StatusCode != http.StatusOK {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "something wrong: %s", string(bodyBytes))
		return
	}

	w.Write(bodyBytes)
}

func (c *Controller) getWelcome(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("welcome from go-postgresql-app"))
}

type Controller struct {
	cli *http.Client
	db  *sqlx.DB
}

func main() {

	// for educational purposes I use hardcoded connection string
	// to run the app via 'go run .' uncomment the string below
	// db, err := sqlx.Connect("pgx", "postgres://postgres:postgres@localhost:5432/postgres?sslmode=disable")
	db, err := sqlx.Connect("pgx", "postgres://postgres:postgres@postgres:5432/postgres?sslmode=disable")
	if err != nil {
		log.Fatal("cannot open postgres:", err)
	}
	driver, err := postgres.WithInstance(db.DB, &postgres.Config{})
	if err != nil {
		log.Fatal("cannot create driver:", err)
	}
	m, err := migrate.NewWithDatabaseInstance("file://migrations", "postgres", driver)
	if err != nil {
		log.Fatal("cannot create db instance:", err)
	}
	err = m.Up()
	if err != nil && !strings.Contains(err.Error(), "no change") {
		log.Fatal("cannot migrate db:", err)
	}

	c := &Controller{
		cli: &http.Client{},
		db:  db,
	}

	r := chi.NewRouter()
	r.Get("/", c.getWelcome)
	r.Get("/test", c.getTest)
	r.Post("/test", c.postTest)
	r.Get("/test/sqlite", c.getTestSQLite)
	r.Post("/test/sqlite", c.postTestSQLite)
	http.ListenAndServe(":80", r)
}
